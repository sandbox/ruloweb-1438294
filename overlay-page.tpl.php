<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $language->language ?>" lang="<?php echo $language->language ?>" dir="<?php echo $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>

  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if lte IE 8]>
    <style type="text/css" media="all">@import "<?php echo $base_path . path_to_theme() ?>/ie.css";</style>
  <![endif]-->
  <!--[if lte IE 6]>
    <style type="text/css" media="all">@import "<?php echo $base_path . path_to_theme() ?>/ie6.css";</style>
  <![endif]-->
  <?php print $scripts; ?>

</head>

<body class="<?php print $body_classes; ?>">

<?php if (!overlay_display_empty_page()): ?>

<?php print $disable_overlay; ?>
<div id="overlay" <?php print $attributes; ?>>
  <div id="overlay-titlebar" class="clearfix">
    <div id="overlay-title-wrapper" class="clearfix">
      <h1 id="overlay-title"<?php print $title_attributes; ?>><?php print $title; ?></h1>
    </div>
    <div id="overlay-close-wrapper">
      <a id="overlay-close" href="#" class="overlay-close"><span class="element-invisible"><?php print t('Close overlay'); ?></span></a>
    </div>
    <?php if ($primary_local_tasks): ?><h2 class="element-invisible"><?php print t('Primary tabs'); ?></h2><ul id="overlay-tabs"><?php print $primary_local_tasks; ?></ul><?php endif; ?>
  </div>
  <div id="overlay-content" class="clearfix"<?php //print $content_attributes; ?>>

  <div id="skip-link">
    <a href="#main-content"><?php print t('Skip to main content'); ?></a>
  </div>

  <div class="element-invisible"><a id="main-content"></a></div>

  <div id="branding" class="clearfix">

    <?php if (!empty($breadcrumb)): ?><?php print $breadcrumb; ?><?php endif; ?>

    <?php /*if (!empty($title)): ?>
      <h1 class="page-title"><?php print $title; ?></h1>
    <?php endif;*/ ?>

    <?php /*if (!empty($primary_local_tasks)): ?><ul class="tabs primary"><?php print $primary_local_tasks; ?></ul><?php endif;*/ ?>

  </div>

  <div id="page">
    <?php if (!empty($secondary_local_tasks)): ?><ul class="tabs secondary"><?php print $secondary_local_tasks; ?></ul><?php endif; ?>

    <div id="content" class="clearfix">

      <?php if (!empty($messages)): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      <?php endif; ?>

      <?php if (!empty($help)): ?>
        <div id="help">
          <?php print $help; ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($action_links)): ?><ul class="action-links"><?php print drupal_render($action_links); ?></ul><?php endif; ?>

      <?php print $content; ?>

    </div>

    <div id="footer">
      <?php if (!empty($feed_icons)): ?><?php print $feed_icons; ?><?php endif; ?>
    </div>

  </div>

  <?php // print $closure; // Prevent admin menu to be displayed. ?>

  </div>
</div>

<?php endif;  ?>

</body>
</html>
